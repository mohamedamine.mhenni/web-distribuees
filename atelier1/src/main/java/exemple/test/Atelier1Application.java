package exemple.test;

import java.util.stream.Stream;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import exemple.test.repository.CandidateRepository;
import  exemple.test.entity.*;

@EnableEurekaClient 
@SpringBootApplication
public class Atelier1Application {

	public static void main(String[] args) {
		SpringApplication.run(Atelier1Application.class, args);
	}
@Bean
ApplicationRunner init (CandidateRepository candidateRep) {
	return args->{
		Stream.of("mariem","sarra","Ahmed").forEach(nom-> {candidateRep.save(new Candidate(nom));
		});
		candidateRep.findAll().forEach(System.out::println);
	};
	
}
}
