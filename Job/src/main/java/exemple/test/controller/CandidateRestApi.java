package exemple.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import exemple.test.entity.Candidate;
import exemple.test.services.CandidatService;

@RestController
@RequestMapping("/api/candidats")
public class CandidateRestApi {
	private String title="Hello tous le monde";
	
	@Autowired
	private CandidatService candidateservice;
	
	@GetMapping("/hello")
	public String sayHello() {
		System.out.println(title);
		return title;
	}
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Candidate> createCandidate(@RequestBody Candidate candidate){
		
		return new ResponseEntity(candidateservice.addCandidate(candidate),HttpStatus.OK);
		
	}
	@PutMapping(value="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Candidate> updateCandidate(@PathVariable int id, @RequestBody Candidate candidate){
		return new ResponseEntity(candidateservice.candidateUpdate(id, candidate),HttpStatus.OK);
		
	}
	
	@DeleteMapping(value = "/{id}",produces =  MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity deleteCandidate(@PathVariable int id) {
		return new ResponseEntity(candidateservice.deleteCandidate(id),HttpStatus.OK);
	}
	
}
