package exemple.test.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import exemple.test.repository.JobRepository;
import exemple.test.entity.*;
@Service
public class JobService {

	@Autowired
	private JobRepository jobrepository;
	
	public Job CreateJob(Job job) {
		return jobrepository.save(job);
	}
	
	public List<Job> findAllJobs(){
		return jobrepository.findAll();
	}
	public Job findJobById(int id){
		if(jobrepository.findById(id).isPresent())
			return jobrepository.findById(id).get();
		else
			return null;
	}
	public Job findJobByService(String  service){
		if(jobrepository.jobByServicename(service).isPresent())
			return jobrepository.jobByServicename(service).get();
		else
			return null;
	}
	
}
