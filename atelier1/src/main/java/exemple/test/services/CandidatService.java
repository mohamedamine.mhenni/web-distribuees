package exemple.test.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import exemple.test.entity.Candidate;
import exemple.test.repository.CandidateRepository;

@Service
public class CandidatService {
	@Autowired
	private CandidateRepository candidateRepo;

	public Candidate addCandidate(Candidate candidate) {
		return candidateRepo.save(candidate);
	}

	public Candidate candidateUpdate(int id, Candidate newCandidate) {

		if (candidateRepo.findById(id).isPresent()) {
			Candidate cand = candidateRepo.findById(id).get();
			cand.setEmail(newCandidate.getEmail());
			cand.setNom(newCandidate.getNom());
			cand.setPrenom(newCandidate.getPrenom());
			return candidateRepo.save(cand);
		} else {
			return null;
		}
	}

	public String deleteCandidate(int id) {
		if (candidateRepo.findById(id).isPresent()) {
			candidateRepo.deleteById(id);
			return ("candidat supprimé");
		} else
			return "candidate not found";
	}
}
