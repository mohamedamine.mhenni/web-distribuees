package exemple.test.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Job implements Serializable{
	@Id
	private int id;
	private String Service;
	private Boolean Etat;
	public Job() {}
	public Job(int id, String service, Boolean etat) {
		super();
		this.id = id;
		Service = service;
		Etat = etat;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getService() {
		return Service;
	}
	public void setService(String service) {
		Service = service;
	}
	public Boolean getEtat() {
		return Etat;
	}
	public void setEtat(Boolean etat) {
		Etat = etat;
	}
	@Override
	public String toString() {
		return "Job [id=" + id + ", Service=" + Service + ", Etat=" + Etat + "]";
	}
	
}
