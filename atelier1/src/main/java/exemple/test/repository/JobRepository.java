package exemple.test.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import exemple.test.entity.Job;



public interface JobRepository extends JpaRepository<Job, Integer>{
	@Query("select j from Job j where j.Service like :service")
	public Page<Job> jobByService(@Param("service") String s, Pageable pageable);
	@Query("select j from Job j where j.Service like :service")
	public Optional<Job> jobByServicename(@Param("service") String s);
}
