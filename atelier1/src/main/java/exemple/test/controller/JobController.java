package exemple.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import exemple.test.services.JobService;
import exemple.test.entity.*;

@RestController
@RequestMapping("/api/jobs")
public class JobController {

	@Autowired
	private JobService jobservice;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Job> createJob(@RequestBody Job job){
		System.out.println(job);
		return new ResponseEntity(jobservice.CreateJob(job),HttpStatus.OK);
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Job>>getAllJobs(){
		return new ResponseEntity(jobservice.findAllJobs(),HttpStatus.OK);
	}
	@GetMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Job>getJobById(@PathVariable int id){
		return new ResponseEntity(jobservice.findJobById(id),HttpStatus.OK);
	}
	@GetMapping(value = "/byService/{service}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Job>getJobByServiceName(@PathVariable String service){
		System.out.println(service);
		return new ResponseEntity(jobservice.findJobByService(service),HttpStatus.OK);
	}

}
